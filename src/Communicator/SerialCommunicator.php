<?php
/*
 * Serial communicator class for communicating with Arduino boards.
 * 
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2016, Alex Fraundorf and/or Snap Programming and Development LLC
 * 
 * @package Snap\ArduinoPHP
 * @version 09/05/2016 0.1.0
 * @since 09/04/2016 0.1.0
 * @license MIT https://opensource.org/licenses/MIT
 * 
 * Important: Place a 10 uF capacitor between the ground (negative) and reset 
 * (positive) pins on the board. Otherwise the serial connection will reset 
 * after every transmission and this will not work.
 * Make sure to remove the capacitor if you need to upload a new arduino sketch.
 */
namespace Snap\ArduinoPHP\Communicator;


// include the php serial class
include_once('includes/php_serial.class.php');
// manually include the interface
include_once('CommunicatorInterface.php');


class SerialCommunicator implements CommunicatorInterface {

    
    /**
     *
     * @var \phpSerial
     */
    public $PhpSerial;
    
    
    /**
     * Sets up serial communicator requirements.
     * 
     * Note: To find your connection port, open up the arduino ide, and open 
     * the serial monitor (Tools -> Serial Monitor).  The serial port being 
     * used will show on the top bar.     
     * 
     * @param string $communication_port ie: '/dev/ttyUSB0', '/dev/ttyS4', etc.
     * @param int $baud_rate - default: 9600
     * @version 09/04/2016 0.1.0
     * @since 09/04/2016 0.1.0
     */
    public function __construct($communication_port, $baud_rate = 9600) {
        // create an instance of the php serial object and store it as an 
        // object property.
        $this->PhpSerial = new \phpSerial();
        // set up the serial connection
        $this->PhpSerial->deviceSet((string) $communication_port);
        // set the baud rate
        $this->PhpSerial->confBaudRate((int) $baud_rate);
    }
    
    
    /**
     * Send the request to the arduino and return its response.
     * 
     * Note: An \ErrorException will be thrown if there is an error.
     * 
     * @param string $request
     * @return string
     * @version 09/05/2016 0.1.0
     * @since 09/04/2016 0.1.0
     */
    public function sendRequest($request) {
        // open the serial connection
        $this->PhpSerial->deviceOpen();
        // send the request
        $this->PhpSerial->sendMessage(trim($request) . "\n");
        // get the response (an exception is thrown if there is an error)
        return (string) $this->getResponse();        
    }
    
    
    /**
     * Get/read the response from the arduino, check it for errors and return 
     * the response.
     * 
     * Note: An \ErrorException will be thrown if there is an error.
     * 
     * @return string
     * @version 09/04/2016 0.1.0
     * @since 09/04/2016 0.1.0
     */
    protected function getResponse() {
        // initialize variables
        $done = false;
        $response = '';
        // loop until we get a line end character
        while(! $done) {
            // read the response
            $read = $this->PhpSerial->readPort();
            // build the response string
            for ($i = 0; $i < strlen($read); $i++) {
                if ($read[$i] == "\n") {
                    $done = true;
                } 
                else {
                    $response .= $read[$i];
                }
            }
        }
        // close the serial connection
        $this->PhpSerial->deviceClose();
        // check for an error condition
        $this->checkForErrors($response);
        // return the response
        return $response;        
    }
    
    
    /**
     * Check the response for an error condition and throw an exception if 
     * one exists.
     * 
     * @param string $response
     * @return void
     * @throws \ErrorException
     * @version 09/04/2016 0.1.0
     * @since 09/04/2016 0.1.0
     */
    protected function checkForErrors($response) {
        // check for 'error' in the response
        if(stripos($response, 'error') !== false) {
            // throw an error exception
            throw new \ErrorException($response);
        }
        return;
    }
    
    
}

<?php
/*
 * Rest API communicator class for communicating with Arduino boards.
 * 
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2016, Alex Fraundorf and/or Snap Programming and Development LLC
 * 
 * @package Snap\ArduinoPHP
 * @version 09/04/2016 0.1.0
 * @since 09/04/2016 0.1.0
 * @license MIT https://opensource.org/licenses/MIT
 * 
 * Important: Place a 10 uF capacitor between the ground (negative) and reset 
 * (positive) pins on the board. Otherwise the serial connection will reset 
 * after every transmission and this will not work.
 */
namespace Snap\ArduinoPHP\Communicator;


class RestCommunicator implements CommunicatorInterface {

    
    public function sendRequest($request);
    
    
}            

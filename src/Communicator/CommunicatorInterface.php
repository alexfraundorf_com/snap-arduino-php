<?php
/*
 * Communicator interface for communicating with Arduino boards.
 * 
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2016, Alex Fraundorf and/or Snap Programming and Development LLC
 * 
 * @package Snap\ArduinoPHP
 * @version 09/04/2016 0.1.0
 * @since 09/04/2016 0.1.0
 * @license MIT https://opensource.org/licenses/MIT
 */
namespace Snap\ArduinoPHP\Communicator;

interface CommunicatorInterface {
    
    
    /**
     * Send the request to the arduino and return its response.
     * 
     * Note: An \ErrorException will be thrown if there is an error.
     * 
     * @param string $request
     * @return string
     */
    public function sendRequest($request);
    
    
}

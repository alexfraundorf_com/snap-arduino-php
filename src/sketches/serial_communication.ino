/*
Arduino Uno Serial Communication
09/03/2016
Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com

Examples:

The serial input string follows the same command schema as the 
Arduino REST API:
https://github.com/marcoschwartz/aREST

===============================================================
Pin Setup:
===============================================================

Setup pin 3 as OUTPUT mode:
mode/3/o 

Setup pin 4 as INPUT mode:
mode/4/i

Setup pin 5 as INPUT_PULLUP mode:
mode/5/ip


===============================================================
Output:
===============================================================

Set a digital pin output value on pin 4 to HIGH (on):
digital/4/1

Set a digital pin output value on pin 5 to LOW (off):
digital/5/0

Set an analog pin output value (0-255) to pin 3:
analog/3/100

Note: When using the digitalOut or analogOut commands, the 
pin is automatically set to output for you.

===============================================================
Input:
===============================================================

Get a digial pin value from pin 8:
digital/8/r

Get an analog pin value from pin A0:
analog/0/r


*/

String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete


void setup() {
  // initialize serial:
  Serial.begin(9600);
  // reserve 200 bytes for the inputString:
  inputString.reserve(200);
  // send ready message
  //Serial.print("ready\n");
}

void loop() {
  // print the string when a newline arrives:
  if (stringComplete) {
    // process the string
    processString(inputString);
    // clear the string:
    inputString = "";
    stringComplete = false;
  }
}


void processString(String str) {

  String action = getValue(str, '/', 0);
  String param1 = getValue(str, '/', 1);
  String param2 = getValue(str, '/', 2);
    
  // setup pin
  if(action == "mode") {
    setupPin(param1, param2, true);  
  }
  // digital pin
  else if(action == "digital") {
    // read from a digital pin
    if(param2 == "r") {
      Serial.println(getDigitalPinValue(param1));
    }
    // write to a digital pin
    else {
      setDigitalPin(param1, param2);
    }   
  }
  // analog pin
  else if(action == "analog") {
    // read from an analog pin
    if(param2 == "r") {
      Serial.println(getAnalogPinValue(param2));
    }
    // write to an analog pin
    else {
      setAnalogPin(param1, param2);
    }   
  }
  else if(action == "") {
    errorResponse("empty action - make sure your request does not have a leading '/'.");
  }  
  else {
    errorResponse("unsupported action (" + action + ")");
  }
}  


void successResponse() {
  Serial.print("success: " + inputString + "\n");
}


void errorResponse(String errorMessage) {
  Serial.print("error: " + inputString + " - " + errorMessage + "\n");
}

/*
Get/read and return a digital pin's value.
*/
int getDigitalPinValue(String pin_id) {
  // set the pin as input
  //setupPin(pin_id, "i", false);
  // get and return the pin's value
  return digitalRead(pin_id.toInt());
}

int getAnalogPinValue(String pin_id) {
  return analogRead(pin_id.toInt());  
}

/*
Sets up a pin's mode.
*/
void setupPin(String pin_id, String pin_setting, boolean send_response) {
  if(pin_setting == "o") {
    pinMode(pin_id.toInt(), OUTPUT);
    if(send_response) {
      successResponse();
    }  
  }
  else if(pin_setting == "i") {
    pinMode(pin_id.toInt(), INPUT);
    if(send_response) {
      successResponse();
    }  
  }
  else if(pin_setting == "ip") {
    pinMode(pin_id.toInt(), INPUT_PULLUP);
    if(send_response) {
      successResponse();
    }  
  }
  else {
    errorResponse("pin setting of '" + pin_setting + "' is not supported");
  }
}

/*
void setPin(String pin_id, String pin_value) {
  setDigitalPin(pin_id, pin_value);  
} 
*/

/*
Sets/writes to a digial output pin.
@param pin_id the number of the pin
@param pin_value "1" or "0"
*/
void setDigitalPin(String pin_id, String pin_value) {
  // set the pin mode
  setupPin(pin_id, "o", false);
  // set the pin value
  if(pin_value == "1") {
    digitalWrite(pin_id.toInt(), HIGH);
    successResponse();
  }
  else if(pin_value == "0") {
    digitalWrite(pin_id.toInt(), LOW);
    successResponse();
  }
  else {
    errorResponse("pin value of '" + pin_value + "' is not supported");  
  }  
}


/*
Sets/writes to an analog output pin.
On the Uno these are pins 3,5,6,9,10,11.
@param pin_id the number of the pin
@param pin_value number 0 - 255
*/
void setAnalogPin(String pin_id, String pin_value) {
  // set up pin
  setupPin(pin_id, "o", false);
  // set pin value
  analogWrite(pin_id.toInt(), pin_value.toInt());
  successResponse();  
}
  
  /*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read(); 
    // add it to the inputString:
    if(inChar != '\n') {
      inputString += inChar;
    }
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    } 
  }
}


String getValue(String data, char separator, int index)
{
 int found = 0;
  int strIndex[] = {
0, -1  };
  int maxIndex = data.length()-1;
  for(int i=0; i<=maxIndex && found<=index; i++){
  if(data.charAt(i)==separator || i==maxIndex){
  found++;
  strIndex[0] = strIndex[1]+1;
  strIndex[1] = (i == maxIndex) ? i+1 : i;
  }
 }
  return found>index ? data.substring(strIndex[0], strIndex[1]) : "";
}



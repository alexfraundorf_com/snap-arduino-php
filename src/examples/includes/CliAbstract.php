<?php
/**
 * CLI application abstract class.
 *
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2016, Alex Fraundorf and/or Snap Programming and Development LLC
 * 
 * @package Snap\Cli
 * @version 09/04/2016 0.1.0
 * @since 06/27/2016  0.1.0
 * @license This software may NOT be used or reproduced in any manner without a commercial license agreement.
 */
namespace Snap\Cli;

// manually include the interface so the autoloader doesn't need to be called
//include_once('CliTaskInterface.php');

abstract class CliAbstract {
    
    
    public function prompt($message) {
        echo PHP_EOL . trim($message) . ' ';
    }
    
    public function successMsg($message) {
        echo PHP_EOL . trim($message) . PHP_EOL;
    }
    
    public function errorMsg($message) {
        echo PHP_EOL . 'Error: ' . trim($message) . PHP_EOL;
    }
    
    public function getInput() {
        return (string) trim(fgets(STDIN));
    }
    
    
    public function clearScreen() {
        passthru('clear');
    }

    
    /*
    public function buildMainMenu() {
        return '
        
    ----------------------------------------------------------------------------
                         Snap Phramework CLI Tool
    ----------------------------------------------------------------------------

        n - Update Application Namespace (current namespace is "' . $this->getNamespace() . '")
        e - Update Environment (current environment is "' . $this->getEnvironment() . '")
            
        c - Create Controller File
        m - Create Model File
        d - Create Basic Domain Object File
        
        x - Create Domain Object and/or Model Files From Database Table
        
        s - Create Service File
        h - Create Helper File
        
        v - Create View Template File
        vs - Create Top and Bottom View Sub-Templates 
        
        g - Remove git files from vendor directory so packages 
            can be part of main repo
        
        C - View Copyright and License
        
        q - Quit

    Please enter your choice: ';
    }
    
    
    public function delegateTask($command) {
        if($command === 'q') {
            $this->quit();
        }
        elseif($command === 'n') {
            $this->updateNamespace();
        }
        elseif($command === 'e') {
            $this->updateEnvironment();
        }
        elseif($command === 'c') {
            $this->createControllerFile();
        }
        elseif($command === 'm') {
            $this->createModelFile();
        }
        elseif($command === 'd') {
            $this->createDomainObjectFile();
        }
        elseif($command === 'x') {
            $this->createDomainObjectAndModelFilesFromTable();
        }
        elseif($command === 'h') {
            $this->createHelperFile();
        }
        elseif($command === 's') {
            $this->createServiceFile();
        }
        elseif($command === 'v') {
            $this->createViewTemplateFile();
        }
        elseif($command === 'vs') {
            $this->createTopAndBottomViewTemplateFiles();
        }
        elseif($command === 'C') {
            $this->viewCopyrightAndLicense();
        }
        elseif($command === 'g') {
            $this->removeVendorGitFiles();
        }
        else {
            $this->invalidCommand($command);
        }
    }
    
     * 
     */
    
    protected function quit() {
        $this->clearScreen();
        exit(0);
        
    }
    
    
}

<?php
/**
 * CLI application abstract class.
 *
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2016, Alex Fraundorf and/or Snap Programming and Development LLC
 * 
 * @package Snap\ArduinoPHP
 * @version 09/05/2016 0.1.0
 * @since 09/04/2016  0.1.0
 * @license MIT https://opensource.org/licenses/MIT
 */
namespace Snap\ArduinoPHP;

// manually require the abstract class
require_once('CliAbstract.php');

class SerialCli extends \Snap\Cli\CliAbstract {
    

    public function welcome() {
        $this->clearScreen();
        echo '
============================================================================
 Welcome to the Serial CLI Arduino Communication Program
    
 Note: Make sure the Arduino IDE and Serial Monitor are closed when using
 this program or they will intercept the responses and this won\'t work!
 
 Important: Place a 10 uF capacitor between the ground (negative) and reset 
 (positive) pins on the board. Otherwise the serial connection will reset 
 after every transmission and this will not work.
 Make sure to remove the capacitor if you need to upload a new arduino sketch.

 Examples:
 
 Set a pin mode: mode/<pin number>/<o or i or ip>
 
 Set/write a digital pin output: digital/<pin number>/<1 or 0> 
 Get/read a digital pin input: digital/<pin number>/r
 
 Set/write an analog pin output: analog/<pin number>/<integer value>
 Get/read an analog pin input: analog/<pin number>/r
 
 Press Ctrl + C to quit
 
 Version 0.1.0 - Package Snap\ArduinoPHP
 Author: Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 Copyright (c) 2016, Alex Fraundorf and/or Snap Programming and Development LLC
 License: MIT https://opensource.org/licenses/MIT
============================================================================
        ';
    }

   
}

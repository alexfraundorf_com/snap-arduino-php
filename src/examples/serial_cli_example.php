#!/usr/bin/php
<?php
/* 
 * Access the arduino serial connection by cli.
 * 
 * Note: On .nix systems you will need to enable this file to be executed. 
 * In a terminal cd to the directory holding this file and type:
 * chmod +x serial_cli_example.php
 * 
 * Instructions:
 * In a terminal cd to the directory holding this file and type:
 * ./serial_cli_example.php
 * 
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2016, Alex Fraundorf and/or Snap Programming and Development LLC
 * 
 * @package Snap\ArduinoPHP
 * @license MIT https://opensource.org/licenses/MIT
 */

// create the cli object
require_once('includes/SerialCli.php');
$Cli = new \Snap\ArduinoPHP\SerialCli();

$Cli->welcome();
$Cli->prompt('Please enter your serial port (default: /dev/ttyUSB0):' . PHP_EOL
        . 'Note: You can find this information in the Arduino IDE Serial Monitor tool.' 
        . PHP_EOL . ':');
$communication_port = $Cli->getInput();
if(!$communication_port) {
    $communication_port = '/dev/ttyUSB0';
}

// create the communicator object
require_once('../Communicator/CommunicatorInterface.php');
require_once('../Communicator/includes/php_serial.class.php');
require_once('../Communicator/SerialCommunicator.php');
$Communicator = new \Snap\ArduinoPHP\Communicator\SerialCommunicator($communication_port);

// create the board object
require_once('../Board/Board.php');
$Board = new \Snap\ArduinoPHP\Board\Board($Communicator);

// loop until cancelled
while(true) {
    
    $Cli->prompt('Enter your command:');
    $request = $Cli->getInput();
    // send the request and echo the response
    echo 'sending...' . PHP_EOL;
    try {
        $Cli->successMsg($Board->sendRequest($request));
    } catch (\Exception $ex) {
        $Cli->errorMsg($ex->getMessage());
    }
    echo PHP_EOL . PHP_EOL;    
    
}
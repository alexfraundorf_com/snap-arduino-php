<?php
/*
 * Arduino Mega board class.
 * 
 * This class does not add any additional functionality to Board. All it does 
 * is add convenience getters and setters that your IDE can auto complete.
 * 
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2016, Alex Fraundorf and/or Snap Programming and Development LLC
 * 
 * @package Snap\ArduinoPHP
 * @version 09/05/2016 0.1.0
 * @since 09/05/2016 0.1.0
 * @license MIT https://opensource.org/licenses/MIT
 */
namespace Snap\ArduinoPHP\Board;

class Mega extends Board {
    
    
    /**
     * Get the value of digital pin 0 which will be 1 (HIGH) or 0 (LOW).
     * 
     * Note: Digital pin 0 is used for receiving data. Using it for something 
     * else may cause problems.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin0() {
        return $this->getDigitalPin(0);
    }
    
    
    /**
     * Get the value of digital pin 1 which will be 1 (HIGH) or 0 (LOW).
     * 
     * Note: Digital pin 1 is used for transmitting data. Using it for something 
     * else may cause problems.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin1() {
        return $this->getDigitalPin(1);
    }
    
    
    /**
     * Get the value of digital pin 2 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin2() {
        return $this->getDigitalPin(2);
    }
    
    
    /**
     * Get the value of digital pin 3 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin3() {
        return $this->getDigitalPin(3);
    }
    
    
    /**
     * Get the value of digital pin 4 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin4() {
        return $this->getDigitalPin(4);
    }
    
    
    /**
     * Get the value of digital pin 5 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin5() {
        return $this->getDigitalPin(5);
    }
    
    
    /**
     * Get the value of digital pin 6 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin6() {
        return $this->getDigitalPin(6);
    }
    
    
    /**
     * Get the value of digital pin 7 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin7() {
        return $this->getDigitalPin(7);
    }
    
    
    /**
     * Get the value of digital pin 8 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin8() {
        return $this->getDigitalPin(8);
    }
    
    
    /**
     * Get the value of digital pin 9 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin9() {
        return $this->getDigitalPin(9);
    }
    
    
    /**
     * Get the value of digital pin 10 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin10() {
        return $this->getDigitalPin(10);
    }
    
    
    /**
     * Get the value of digital pin 11 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin11() {
        return $this->getDigitalPin(11);
    }
    
    
    /**
     * Get the value of digital pin 12 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin12() {
        return $this->getDigitalPin(12);
    }
    
    
    /**
     * Get the value of digital pin 13 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin13() {
        return $this->getDigitalPin(13);
    }
    
    
    /**
     * Get the value of digital pin 14 which will be 1 (HIGH) or 0 (LOW).
     * 
     * Note: Digital pin 14 is used for transmitting data. Using it for 
     * something else may cause problems.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin14() {
        return $this->getDigitalPin(14);
    }
    
    
    /**
     * Get the value of digital pin 15 which will be 1 (HIGH) or 0 (LOW).
     * 
     * Note: Digital pin 15 is used for receiving data. Using it for 
     * something else may cause problems.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin15() {
        return $this->getDigitalPin(15);
    }
    
    
    /**
     * Get the value of digital pin 16 which will be 1 (HIGH) or 0 (LOW).
     * 
     * Note: Digital pin 16 is used for transmitting data. Using it for 
     * something else may cause problems.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin16() {
        return $this->getDigitalPin(16);
    }
    
    
    /**
     * Get the value of digital pin 17 which will be 1 (HIGH) or 0 (LOW).
     * 
     * Note: Digital pin 17 is used for receiving data. Using it for 
     * something else may cause problems.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin17() {
        return $this->getDigitalPin(17);
    }
    
    
    /**
     * Get the value of digital pin 18 which will be 1 (HIGH) or 0 (LOW).
     * 
     * Note: Digital pin 18 is used for receiving data. Using it for 
     * something else may cause problems.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin18() {
        return $this->getDigitalPin(18);
    }
    
    
    /**
     * Get the value of digital pin 19 which will be 1 (HIGH) or 0 (LOW).
     * 
     * Note: Digital pin 19 is used for transmitting data. Using it for 
     * something else may cause problems.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin19() {
        return $this->getDigitalPin(19);
    }
    
    
    /**
     * Get the value of digital pin 20 which will be 1 (HIGH) or 0 (LOW).
     * 
     * Note: Digital pin 20 is used for SDA. Using it for something else may 
     * cause problems.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin20() {
        return $this->getDigitalPin(20);
    }
    
    
    /**
     * Get the value of digital pin 21 which will be 1 (HIGH) or 0 (LOW).
     * 
     * Note: Digital pin 21 is used for SCL. Using it for something else may 
     * cause problems.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin21() {
        return $this->getDigitalPin(21);
    }
    
    
    /**
     * Get the value of digital pin 22 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin22() {
        return $this->getDigitalPin(22);
    }
    
    
    /**
     * Get the value of digital pin 23 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin23() {
        return $this->getDigitalPin(23);
    }
    
    
    /**
     * Get the value of digital pin 24 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin24() {
        return $this->getDigitalPin(24);
    }
    
    
    /**
     * Get the value of digital pin 25 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin25() {
        return $this->getDigitalPin(25);
    }
    
    
    /**
     * Get the value of digital pin 26 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin26() {
        return $this->getDigitalPin(26);
    }
    
    
    /**
     * Get the value of digital pin 27 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin27() {
        return $this->getDigitalPin(27);
    }
    
    
    /**
     * Get the value of digital pin 28 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin28() {
        return $this->getDigitalPin(28);
    }
    
    
    /**
     * Get the value of digital pin 29 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin29() {
        return $this->getDigitalPin(29);
    }
    
    
    /**
     * Get the value of digital pin 30 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin30() {
        return $this->getDigitalPin(30);
    }
    
    
    /**
     * Get the value of digital pin 31 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin31() {
        return $this->getDigitalPin(31);
    }
    
    
    /**
     * Get the value of digital pin 32 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin32() {
        return $this->getDigitalPin(32);
    }
    
    
    /**
     * Get the value of digital pin 33 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin33() {
        return $this->getDigitalPin(33);
    }
    
    
    /**
     * Get the value of digital pin 34 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin34() {
        return $this->getDigitalPin(34);
    }
    
    
    /**
     * Get the value of digital pin 35 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin35() {
        return $this->getDigitalPin(35);
    }
    
    
    /**
     * Get the value of digital pin 36 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin36() {
        return $this->getDigitalPin(36);
    }
    
    
    /**
     * Get the value of digital pin 37 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin37() {
        return $this->getDigitalPin(37);
    }
    
    
    /**
     * Get the value of digital pin 38 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin38() {
        return $this->getDigitalPin(38);
    }
    
    
    /**
     * Get the value of digital pin 39 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin39() {
        return $this->getDigitalPin(39);
    }
    
    
    /**
     * Get the value of digital pin 40 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin40() {
        return $this->getDigitalPin(40);
    }
    
    
    /**
     * Get the value of digital pin 41 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin41() {
        return $this->getDigitalPin(41);
    }
    
    
    /**
     * Get the value of digital pin 42 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin42() {
        return $this->getDigitalPin(42);
    }
    
    
    /**
     * Get the value of digital pin 43 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin43() {
        return $this->getDigitalPin(43);
    }
    
    
    /**
     * Get the value of digital pin 44 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin44() {
        return $this->getDigitalPin(44);
    }
    
    
    /**
     * Get the value of digital pin 45 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin45() {
        return $this->getDigitalPin(45);
    }
    
    
    /**
     * Get the value of digital pin 46 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin46() {
        return $this->getDigitalPin(46);
    }
    
    
    /**
     * Get the value of digital pin 47 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin47() {
        return $this->getDigitalPin(47);
    }
    
    
    /**
     * Get the value of digital pin 48 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin48() {
        return $this->getDigitalPin(48);
    }
    
    
    /**
     * Get the value of digital pin 49 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin49() {
        return $this->getDigitalPin(49);
    }
    
    
    /**
     * Get the value of digital pin 50 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin50() {
        return $this->getDigitalPin(50);
    }
    
    
    /**
     * Get the value of digital pin 51 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin51() {
        return $this->getDigitalPin(51);
    }
    
    
    /**
     * Get the value of digital pin 52 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin52() {
        return $this->getDigitalPin(52);
    }
    
    
    /**
     * Get the value of digital pin 53 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin53() {
        return $this->getDigitalPin(53);
    }

    
    /**
     * Set the value of digital pin 0.
     * 
     * Note: Digital pin 0 is used for receiving data. Using it for something 
     * else may cause problems.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin0($value) {
        return $this->setDigitalPin(0, $value);
    }
    
    
    /**
     * Set the value of digital pin 1.
     * 
     * Note: Digital pin 1 is used for transmitting data. Using it for something 
     * else may cause problems.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin1($value) {
        return $this->setDigitalPin(1, $value);
    }
    
    
    /**
     * Set the value of digital pin 2.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin2($value) {
        return $this->setDigitalPin(2, $value);
    }
    
    
    /**
     * Set the value of digital pin 3.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin3($value) {
        return $this->setDigitalPin(3, $value);
    }
    
    
    /**
     * Set the value of digital pin 4.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin4($value) {
        return $this->setDigitalPin(4, $value);
    }
    
    
    /**
     * Set the value of digital pin 5.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin5($value) {
        return $this->setDigitalPin(5, $value);
    }
    
    
    /**
     * Set the value of digital pin 6.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin6($value) {
        return $this->setDigitalPin(6, $value);
    }
    
    
    /**
     * Set the value of digital pin 7.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin7($value) {
        return $this->setDigitalPin(7, $value);
    }
    
    
    /**
     * Set the value of digital pin 8.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin8($value) {
        return $this->setDigitalPin(8, $value);
    }
    
    
    /**
     * Set the value of digital pin 9.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin9($value) {
        return $this->setDigitalPin(9, $value);
    }
    
    
    /**
     * Set the value of digital pin 10.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin10($value) {
        return $this->setDigitalPin(10, $value);
    }
    
    
    /**
     * Set the value of digital pin 11.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin11($value) {
        return $this->setDigitalPin(11, $value);
    }
    
    
    /**
     * Set the value of digital pin 12.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin12($value) {
        return $this->setDigitalPin(12, $value);
    }
    
    
    /**
     * Set the value of digital pin 13.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin13($value) {
        return $this->setDigitalPin(13, $value);
    }
    
    
    /**
     * Set the value of digital pin 14.
     * 
     * Note: Digital pin 14 is used for transmitting data. Using it for 
     * something else may cause problems.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin14($value) {
        return $this->setDigitalPin(14, $value);
    }
    
    
    /**
     * Set the value of digital pin 15.
     * 
     * Note: Digital pin 15 is used for receiving data. Using it for 
     * something else may cause problems.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin15($value) {
        return $this->setDigitalPin(15, $value);
    }
    
    
    /**
     * Set the value of digital pin 16.
     * 
     * Note: Digital pin 16 is used for transmitting data. Using it for 
     * something else may cause problems.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin16($value) {
        return $this->setDigitalPin(16, $value);
    }
    
    
    /**
     * Set the value of digital pin 17.
     * 
     * Note: Digital pin 17 is used for receiving data. Using it for 
     * something else may cause problems.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin17($value) {
        return $this->setDigitalPin(17, $value);
    }
    
    
    /**
     * Set the value of digital pin 18.
     * 
     * Note: Digital pin 18 is used for receiving data. Using it for 
     * something else may cause problems.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin18($value) {
        return $this->setDigitalPin(18, $value);
    }
    
    
    /**
     * Set the value of digital pin 19.
     * 
     * Note: Digital pin 19 is used for transmitting data. Using it for 
     * something else may cause problems.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin19($value) {
        return $this->setDigitalPin(19, $value);
    }
    
    
    /**
     * Set the value of digital pin 20.
     * 
     * Note: Digital pin 20 is used for SDA. Using it for something else may 
     * cause problems.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin20($value) {
        return $this->setDigitalPin(20, $value);
    }
    
    
    /**
     * Set the value of digital pin 21.
     * 
     * Note: Digital pin 21 is used for SCL. Using it for something else may 
     * cause problems.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin21($value) {
        return $this->setDigitalPin(21, $value);
    }
    
    
    /**
     * Set the value of digital pin 22.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin22($value) {
        return $this->setDigitalPin(22, $value);
    }
    
    
    /**
     * Set the value of digital pin 23.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin23($value) {
        return $this->setDigitalPin(23, $value);
    }
    
    
    /**
     * Set the value of digital pin 24.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin24($value) {
        return $this->setDigitalPin(24, $value);
    }
    
    
    /**
     * Set the value of digital pin 25.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin25($value) {
        return $this->setDigitalPin(25, $value);
    }
    
    
    /**
     * Set the value of digital pin 26.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin26($value) {
        return $this->setDigitalPin(26, $value);
    }
    
    
    /**
     * Set the value of digital pin 27.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin27($value) {
        return $this->setDigitalPin(27, $value);
    }
    
    
    /**
     * Set the value of digital pin 28.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin28($value) {
        return $this->setDigitalPin(28, $value);
    }
    
    
    /**
     * Set the value of digital pin 29.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin29($value) {
        return $this->setDigitalPin(29, $value);
    }
    
    
    /**
     * Set the value of digital pin 30.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin30($value) {
        return $this->setDigitalPin(30, $value);
    }
    
    
    /**
     * Set the value of digital pin 31.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin31($value) {
        return $this->setDigitalPin(31, $value);
    }
    
    
    /**
     * Set the value of digital pin 32.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin32($value) {
        return $this->setDigitalPin(32, $value);
    }
    
    
    /**
     * Set the value of digital pin 33.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin33($value) {
        return $this->setDigitalPin(33, $value);
    }
    
    
    /**
     * Set the value of digital pin 34.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin34($value) {
        return $this->setDigitalPin(34, $value);
    }
    
    
    /**
     * Set the value of digital pin 35.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin35($value) {
        return $this->setDigitalPin(35, $value);
    }
    
    
    /**
     * Set the value of digital pin 36.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin36($value) {
        return $this->setDigitalPin(36, $value);
    }
    
    
    /**
     * Set the value of digital pin 37.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin37($value) {
        return $this->setDigitalPin(37, $value);
    }
    
    
    /**
     * Set the value of digital pin 38.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin38($value) {
        return $this->setDigitalPin(38, $value);
    }
    
    
    /**
     * Set the value of digital pin 39.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin39($value) {
        return $this->setDigitalPin(39, $value);
    }
    
    
    /**
     * Set the value of digital pin 40.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin40($value) {
        return $this->setDigitalPin(40, $value);
    }
    
    
    /**
     * Set the value of digital pin 41.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin41($value) {
        return $this->setDigitalPin(41, $value);
    }
    
    
    /**
     * Set the value of digital pin 42.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin42($value) {
        return $this->setDigitalPin(42, $value);
    }
    
    
    /**
     * Set the value of digital pin 43.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin43($value) {
        return $this->setDigitalPin(43, $value);
    }
    
    
    /**
     * Set the value of digital pin 44.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin44($value) {
        return $this->setDigitalPin(44, $value);
    }
    
    
    /**
     * Set the value of digital pin 45.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin45($value) {
        return $this->setDigitalPin(45, $value);
    }
    
    
    /**
     * Set the value of digital pin 46.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin46($value) {
        return $this->setDigitalPin(46, $value);
    }
    
    
    /**
     * Set the value of digital pin 47.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin47($value) {
        return $this->setDigitalPin(47, $value);
    }
    
    
    /**
     * Set the value of digital pin 48.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin48($value) {
        return $this->setDigitalPin(48, $value);
    }
    
    
    /**
     * Set the value of digital pin 49.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin49($value) {
        return $this->setDigitalPin(49, $value);
    }
    
    
    /**
     * Set the value of digital pin 50.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin50($value) {
        return $this->setDigitalPin(50, $value);
    }
    
    
    /**
     * Set the value of digital pin 51.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin51($value) {
        return $this->setDigitalPin(51, $value);
    }
    
    
    /**
     * Set the value of digital pin 52.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin52($value) {
        return $this->setDigitalPin(52, $value);
    }
    
    
    /**
     * Set the value of digital pin 53.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin53($value) {
        return $this->setDigitalPin(53, $value);
    }
    
    
    /**
     * Get the value of analog pin 0.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getAnalogPin0() {
        return $this->getAnalogPin(0);
    }
    
    
    /**
     * Get the value of analog pin 1.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getAnalogPin1() {
        return $this->getAnalogPin(1);
    }
    
    
    /**
     * Get the value of analog pin 2.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getAnalogPin2() {
        return $this->getAnalogPin(2);
    }
    
    
    /**
     * Get the value of analog pin 3.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getAnalogPin3() {
        return $this->getAnalogPin(3);
    }
    
    
    /**
     * Get the value of analog pin 4.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getAnalogPin4() {
        return $this->getAnalogPin(4);
    }
    
    
    /**
     * Get the value of analog pin 5.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getAnalogPin5() {
        return $this->getAnalogPin(5);
    }
    
    
    /**
     * Get the value of analog pin 6.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getAnalogPin6() {
        return $this->getAnalogPin(6);
    }
    
    
    /**
     * Get the value of analog pin 7.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getAnalogPin7() {
        return $this->getAnalogPin(7);
    }
    
    
    /**
     * Get the value of analog pin 8.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getAnalogPin8() {
        return $this->getAnalogPin(8);
    }
    
    
    /**
     * Get the value of analog pin 9.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getAnalogPin9() {
        return $this->getAnalogPin(9);
    }
    
    
    /**
     * Get the value of analog pin 10.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getAnalogPin10() {
        return $this->getAnalogPin(10);
    }
    
    
    /**
     * Get the value of analog pin 11.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getAnalogPin11() {
        return $this->getAnalogPin(11);
    }
    
    
    /**
     * Get the value of analog pin 12.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getAnalogPin12() {
        return $this->getAnalogPin(12);
    }
    
    
    /**
     * Get the value of analog pin 13.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getAnalogPin13() {
        return $this->getAnalogPin(13);
    }
    
    
    /**
     * Get the value of analog pin 14.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getAnalogPin14() {
        return $this->getAnalogPin(14);
    }
    
    
    /**
     * Get the value of analog pin 15.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getAnalogPin15() {
        return $this->getAnalogPin(15);
    }
    
    
    /**
     * Set the value of analog (PWM) pin 0.
     * 
     * @param int $value 
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setAnalogPin0($value) {
        return $this->setAnalogPin(0, $value);
    }
    
    
    /**
     * Set the value of analog (PWM) pin 1.
     * 
     * @param int $value 
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setAnalogPin1($value) {
        return $this->setAnalogPin(1, $value);
    }
    
    
    /**
     * Set the value of analog (PWM) pin 2.
     * 
     * @param int $value 
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setAnalogPin2($value) {
        return $this->setAnalogPin(2, $value);
    }
    
    
    /**
     * Set the value of analog (PWM) pin 3.
     * 
     * @param int $value 
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setAnalogPin3($value) {
        return $this->setAnalogPin(3, $value);
    }
    
    
    /**
     * Set the value of analog (PWM) pin 4.
     * 
     * @param int $value 
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setAnalogPin4($value) {
        return $this->setAnalogPin(4, $value);
    }
    
    
    /**
     * Set the value of analog (PWM) pin 5.
     * 
     * @param int $value 
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setAnalogPin5($value) {
        return $this->setAnalogPin(5, $value);
    }
    
    
    /**
     * Set the value of analog (PWM) pin 6.
     * 
     * @param int $value 
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setAnalogPin6($value) {
        return $this->setAnalogPin(6, $value);
    }
    
    
    /**
     * Set the value of analog (PWM) pin 7.
     * 
     * @param int $value 
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setAnalogPin7($value) {
        return $this->setAnalogPin(7, $value);
    }
    
    
    /**
     * Set the value of analog (PWM) pin 8.
     * 
     * @param int $value 
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setAnalogPin8($value) {
        return $this->setAnalogPin(8, $value);
    }
    
    
    /**
     * Set the value of analog (PWM) pin 9.
     * 
     * @param int $value 
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setAnalogPin9($value) {
        return $this->setAnalogPin(9, $value);
    }
    
    
    /**
     * Set the value of analog (PWM) pin 10.
     * 
     * @param int $value 
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setAnalogPin10($value) {
        return $this->setAnalogPin(10, $value);
    }
    
    
    /**
     * Set the value of analog (PWM) pin 11.
     * 
     * @param int $value 
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setAnalogPin11($value) {
        return $this->setAnalogPin(11, $value);
    }
    
    
    /**
     * Set the value of analog (PWM) pin 12.
     * 
     * @param int $value 
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setAnalogPin12($value) {
        return $this->setAnalogPin(12, $value);
    }
    
    
    /**
     * Set the value of analog (PWM) pin 13.
     * 
     * @param int $value 
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setAnalogPin13($value) {
        return $this->setAnalogPin(13, $value);
    }
    
    
}

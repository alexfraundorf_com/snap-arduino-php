<?php
/*
 * Basic (base) Arduino board class.
 * 
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2016, Alex Fraundorf and/or Snap Programming and Development LLC
 * 
 * @package Snap\ArduinoPHP
 * @version 09/05/2016 0.1.0
 * @since 09/04/2016 0.1.0
 * @license MIT https://opensource.org/licenses/MIT
 */
namespace Snap\ArduinoPHP\Board;

// manually include the interface so the autoloader doesn't need to
include_once('BoardInterface.php');

class Board implements BoardInterface {
    
    /**
     *
     * @var \Snap\ArduinoPHP\Communicator\CommunicatorInterface
     */
    protected $Communicator;
    
    
    /**
     * Accepts a communicator object and sets up the board object.
     * 
     * @param \Snap\ArduinoPHP\Communicator\CommunicatorInterface $Communicator
     * @version 09/04/2016 0.1.0
     * @since 09/04/2016 0.1.0
     */
    public function __construct(\Snap\ArduinoPHP\Communicator\CommunicatorInterface $Communicator) {
        $this->Communicator = $Communicator;
    }
    
    
    /**
     * Set a pin mode as "output"/"o", "input"/"i" or "input_pullup"/"ip".
     * 
     * @param int $pin_id the id of the pin to set the mode on.
     * @param string $mode the mode to set
     * @return bool
     * @throws \InvalidArgumentException
     * @version 09/04/2016 0.1.0
     * @since 09/04/2016 0.1.0
     */
    public function setPinMode($pin_id, $mode) {
        // translate
        if($mode === 'output') {
            $mode = 'o';
        }
        elseif($mode === 'input') {
            $mode = 'i';
        }
        elseif($mode === 'input_pullup') {
            $mode = 'ip';
        }
        // validate mode
        if($mode !== 'o' && $mode !== 'i' && $mode !== 'ip') {
            throw new \InvalidArgumentException('Unsupported value for mode: ' . $mode);
        }
        // build the request
        $request = 'mode/' . (int) $pin_id . '/' . $mode;
        // send the message and return the response
        return (bool) $this->sendRequest($request);
    }
    
    
    /**
     * Set/write an analog output pin value and return true on success.
     * 
     * @param int $pin_id
     * @param int $value
     * @return bool
     * @version 09/04/2016 0.1.0
     * @since 09/04/2016 0.1.0
     */
    public function setAnalogPin($pin_id, $value) {
        // build the request
        $request = 'analog/' . (int) $pin_id . '/' . (int) $value;
        // send the message and return the response
        return (bool) $this->sendRequest($request);
    }
    
    
    /**
     * Get/read the value (int) of an analog pin input.
     * 
     * @param int $pin_id
     * @return int
     * @version 09/04/2016 0.1.0
     * @since 09/04/2016 0.1.0
     */
    public function getAnalogPin($pin_id) {
        // build the request
        $request = 'analog/' . (int) $pin_id . '/r';
        // send the message and return the response
        return (int) $this->sendRequest($request);
    }
    
    
    /**
     * Set/write a digital output pin value and return true on success.
     * 
     * @param int $pin_id
     * @param int $value
     * @return bool
     * @throws \InvalidArgumentException
     * @version 09/04/2016 0.1.0
     * @since 09/04/2016 0.1.0
     */
    public function setDigitalPin($pin_id, $value) {
        // validate value
        $value = (int) $value;
        if($value !== 1 && $value !== 0) {
            throw new \InvalidArgumentException('Value for setting a digital '
                    . 'pin must be 1 or 0.');
        }
        // build the request
        $request = 'digital/' . (int) $pin_id . '/' . $value;
        // send the message and return the response
        return (bool) $this->sendRequest($request);
    }
    
    
    /**
     * Get/read the value (int) of a digital pin input and return 1 or 0.
     * 
     * @param int $pin_id
     * @return int
     * @version 09/04/2016 0.1.0
     * @since 09/04/2016 0.1.0
     */
    public function getDigitalPin($pin_id) {
        // build the request
        $request = 'digital/' . (int) $pin_id . '/r';
        // send the message and return the response
        return (int) $this->sendRequest($request);
    }
    

    /**
     * Send the request to the arduino board (using the Communicator object) 
     * and return the response.
     * 
     * @param string $request
     * @return bool|int
     * @version 09/04/2016 0.1.0
     * @since 09/04/2016 0.1.0
     */
    public function sendRequest($request) {
        return $this->Communicator->sendRequest($request);
    }
    
    
}

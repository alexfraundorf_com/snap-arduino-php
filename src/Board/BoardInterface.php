<?php
/*
 * Arduino board interface.
 * 
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2016, Alex Fraundorf and/or Snap Programming and Development LLC
 * 
 * @package Snap\ArduinoPHP
 * @version 09/05/2016 0.1.0
 * @since 09/05/2016 0.1.0
 * @license MIT https://opensource.org/licenses/MIT
 */
namespace Snap\ArduinoPHP\Board;

interface BoardInterface {
    
    /**
     * @var sets a value of 1 for HIGH
     */
    const HIGH = 1;
    
    /**
     * @var sets a value of 0 for LOW
     */
    const LOW = 0;
    
    
    /**
     * Accepts a communicator object and sets up the board object.
     * 
     * @param \Snap\ArduinoPHP\Communicator\CommunicatorInterface $Communicator
     */
    public function __construct(\Snap\ArduinoPHP\Communicator\CommunicatorInterface $Communicator);
    
    
    /**
     * Set a pin mode as "output"/"o", "input"/"i" or "input_pullup"/"ip".
     * 
     * @param int $pin_id the id of the pin to set the mode on.
     * @param string $mode the mode to set
     * @return bool
     * @throws \InvalidArgumentException
     */
    public function setPinMode($pin_id, $mode);
    
    
    /**
     * Set/write an analog output pin value and return true on success.
     * 
     * @param int $pin_id
     * @param int $value
     * @return bool
     */
    public function setAnalogPin($pin_id, $value);
    
    
    /**
     * Get/read the value (int) of an analog pin input.
     * 
     * @param int $pin_id
     * @return int
     */
    public function getAnalogPin($pin_id);
    
    
    /**
     * Set/write a digital output pin value and return true on success.
     * 
     * @param int $pin_id
     * @param int $value
     * @return bool
     * @throws \InvalidArgumentException
     */
    public function setDigitalPin($pin_id, $value);
    
    
    /**
     * Get/read the value (int) of a digital pin input and return 1 or 0.
     * 
     * @param int $pin_id
     * @return int
     */
    public function getDigitalPin($pin_id);
    

    /**
     * Send the request to the arduino board (using the Communicator object) 
     * and return the response.
     * 
     * @param string $request
     * @return bool|int
     */
    public function sendRequest($request);
    
    
}

<?php
/*
 * Arduino Nano board class.
 * 
 * This class does not add any additional functionality to Board. All it does 
 * is add convenience getters and setters that your IDE can auto complete.
 * 
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2016, Alex Fraundorf and/or Snap Programming and Development LLC
 * 
 * @package Snap\ArduinoPHP
 * @version 09/05/2016 0.1.0
 * @since 09/05/2016 0.1.0
 * @license MIT https://opensource.org/licenses/MIT
 */
namespace Snap\ArduinoPHP\Board;

class Nano extends Board {
    
    
    /**
     * Get the value of digital pin 0 which will be 1 (HIGH) or 0 (LOW).
     * 
     * Note: Digital pin 0 is used for receiving data. Using it for something 
     * else may cause problems.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin0() {
        return $this->getDigitalPin(0);
    }
    
    
    /**
     * Get the value of digital pin 1 which will be 1 (HIGH) or 0 (LOW).
     * 
     * Note: Digital pin 1 is used for transmitting data. Using it for something 
     * else may cause problems.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin1() {
        return $this->getDigitalPin(1);
    }
    
    
    /**
     * Get the value of digital pin 2 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin2() {
        return $this->getDigitalPin(2);
    }
    
    
    /**
     * Get the value of digital pin 3 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin3() {
        return $this->getDigitalPin(3);
    }
    
    
    /**
     * Get the value of digital pin 4 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin4() {
        return $this->getDigitalPin(4);
    }
    
    
    /**
     * Get the value of digital pin 5 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin5() {
        return $this->getDigitalPin(5);
    }
    
    
    /**
     * Get the value of digital pin 6 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin6() {
        return $this->getDigitalPin(6);
    }
    
    
    /**
     * Get the value of digital pin 7 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin7() {
        return $this->getDigitalPin(7);
    }
    
    
    /**
     * Get the value of digital pin 8 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin8() {
        return $this->getDigitalPin(8);
    }
    
    
    /**
     * Get the value of digital pin 9 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin9() {
        return $this->getDigitalPin(9);
    }
    
    
    /**
     * Get the value of digital pin 10 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin10() {
        return $this->getDigitalPin(10);
    }
    
    
    /**
     * Get the value of digital pin 11 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin11() {
        return $this->getDigitalPin(11);
    }
    
    
    /**
     * Get the value of digital pin 12 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin12() {
        return $this->getDigitalPin(12);
    }
    
    
    /**
     * Get the value of digital pin 13 which will be 1 (HIGH) or 0 (LOW).
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getDigitalPin13() {
        return $this->getDigitalPin(13);
    }

    
    /**
     * Set the value of digital pin 0.
     * 
     * Note: Digital pin 0 is used for receiving data. Using it for something 
     * else may cause problems.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin0($value) {
        return $this->setDigitalPin(0, $value);
    }
    
    
    /**
     * Set the value of digital pin 1.
     * 
     * Note: Digital pin 1 is used for transmitting data. Using it for something 
     * else may cause problems.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin1($value) {
        return $this->setDigitalPin(1, $value);
    }
    
    
    /**
     * Set the value of digital pin 2.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin2($value) {
        return $this->setDigitalPin(2, $value);
    }
    
    
    /**
     * Set the value of digital pin 3.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin3($value) {
        return $this->setDigitalPin(3, $value);
    }
    
    
    /**
     * Set the value of digital pin 4.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin4($value) {
        return $this->setDigitalPin(4, $value);
    }
    
    
    /**
     * Set the value of digital pin 5.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin5($value) {
        return $this->setDigitalPin(5, $value);
    }
    
    
    /**
     * Set the value of digital pin 6.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin6($value) {
        return $this->setDigitalPin(6, $value);
    }
    
    
    /**
     * Set the value of digital pin 7.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin7($value) {
        return $this->setDigitalPin(7, $value);
    }
    
    
    /**
     * Set the value of digital pin 8.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin8($value) {
        return $this->setDigitalPin(8, $value);
    }
    
    
    /**
     * Set the value of digital pin 9.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin9($value) {
        return $this->setDigitalPin(9, $value);
    }
    
    
    /**
     * Set the value of digital pin 10.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin10($value) {
        return $this->setDigitalPin(10, $value);
    }
    
    
    /**
     * Set the value of digital pin 11.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin11($value) {
        return $this->setDigitalPin(11, $value);
    }
    
    
    /**
     * Set the value of digital pin 12.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin12($value) {
        return $this->setDigitalPin(12, $value);
    }
    
    
    /**
     * Set the value of digital pin 13.
     * 
     * @param int $value 1 or 0 or constant HIGH or LOW
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setDigitalPin13($value) {
        return $this->setDigitalPin(13, $value);
    }
    
    
    /**
     * Get the value of analog pin 0.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getAnalogPin0() {
        return $this->getAnalogPin(0);
    }
    
    
    /**
     * Get the value of analog pin 1.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getAnalogPin1() {
        return $this->getAnalogPin(1);
    }
    
    
    /**
     * Get the value of analog pin 2.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getAnalogPin2() {
        return $this->getAnalogPin(2);
    }
    
    
    /**
     * Get the value of analog pin 3.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getAnalogPin3() {
        return $this->getAnalogPin(3);
    }
    
    
    /**
     * Get the value of analog pin 4.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getAnalogPin4() {
        return $this->getAnalogPin(4);
    }
    
    
    /**
     * Get the value of analog pin 5.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getAnalogPin5() {
        return $this->getAnalogPin(5);
    }
    
    
    /**
     * Get the value of analog pin 6.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getAnalogPin6() {
        return $this->getAnalogPin(6);
    }
    
    
    /**
     * Get the value of analog pin 7.
     * 
     * @return int
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function getAnalogPin7() {
        return $this->getAnalogPin(7);
    }
    
    
    /**
     * Set the value of analog (PWM) pin 3.
     * 
     * @param int $value 
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setAnalogPin3($value) {
        return $this->setAnalogPin(3, $value);
    }
    
    
    /**
     * Set the value of analog (PWM) pin 5.
     * 
     * @param int $value 
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setAnalogPin5($value) {
        return $this->setAnalogPin(5, $value);
    }
    
    
    /**
     * Set the value of analog (PWM) pin 6.
     * 
     * @param int $value 
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setAnalogPin6($value) {
        return $this->setAnalogPin(6, $value);
    }
    
    
    /**
     * Set the value of analog (PWM) pin 9.
     * 
     * @param int $value 
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setAnalogPin9($value) {
        return $this->setAnalogPin(9, $value);
    }
    
    
    /**
     * Set the value of analog (PWM) pin 10.
     * 
     * @param int $value 
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setAnalogPin10($value) {
        return $this->setAnalogPin(10, $value);
    }
    
    
    /**
     * Set the value of analog (PWM) pin 11.
     * 
     * @param int $value 
     * @return bool
     * @version 09/05/2016 0.1.0
     * @since 09/05/2016 0.1.0
     */
    public function setAnalogPin11($value) {
        return $this->setAnalogPin(11, $value);
    }
    
    
}
